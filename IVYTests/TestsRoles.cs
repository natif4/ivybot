﻿using Discord;
using IVY.Objets;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace IVYTests
{
    /// <summary>
    /// Classe de tests concernant les rôles
    /// </summary>
    [TestClass]
    public class TestsRoles
    {
        #region Tests sur ObtenirRoles()
        /// <summary>
        /// Teste que la méthode retourne la liste des rôles d'un membre.
        /// </summary>
        [TestMethod]
        public void TestObtenirRolesSucces()
        {
            try
            {
                var role1 = new Mock<IRole>();
                role1.Setup(x => x.Id).Returns(1);

                var role2 = new Mock<IRole>();
                role2.Setup(x => x.Id).Returns(2);

                var membre = new Mock<IGuildUser>();
                membre.Setup(x => x.Id).Returns(10);

                Role role = new();
                role.AjouterMembreRole(membre.Object.Id);
                role.AjouterRole(membre.Object, role1.Object);
                role.AjouterRole(membre.Object, role2.Object);
                Assert.IsTrue(role.ObtenirRoles(membre.Object.Id).Count == 2);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste que la liste est initialement vide et non nulle.
        /// </summary>
        [TestMethod]
        public void TestObtenirRolesInitialeVide()
        {
            try
            {
                Role role = new();
                role.AjouterMembreRole(10);
                Assert.IsTrue(role.ObtenirRoles(10).Count == 0);
            }
            catch (ArgumentException) { }
        }
        #endregion

        #region Tests sur ViderRoles()
        /// <summary>
        /// Teste que la méthode enlève les rôles d'un membre.
        /// </summary>
        [TestMethod]
        public void TestViderRolesSucces()
        {
            try
            {
                var role1 = new Mock<IRole>();
                role1.Setup(x => x.Id).Returns(1);

                var membre = new Mock<IGuildUser>();
                membre.Setup(x => x.Id).Returns(10);

                Role role = new();
                role.AjouterMembreRole(membre.Object.Id);
                role.AjouterRole(membre.Object, role1.Object);
                Assert.IsTrue(role.ObtenirRoles(membre.Object.Id).Count == 1);

                role.ViderRoles(membre.Object.Id);
                Assert.IsTrue(role.ObtenirRoles(membre.Object.Id).Count == 0);
            }
            catch (ArgumentException) { }
        }
        
        /// <summary>
        /// Teste le comportement de la méthode avec une entrée nulle.
        /// </summary>
        [TestMethod]
        public void TestViderRolesNull()
        {
            try
            {
                Role role = new();

                int? x = null;

                Action action = () => role.ViderRoles((ulong) x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }
        #endregion

        #region Tests sur EstDansSystemeRole()
        /// <summary>
        /// Teste que l'identifiant est dans le système seulement quand il est ajouté.
        /// </summary>
        [TestMethod]
        public void TestEstDansSystemeRoleSucces()
        {
            try
            {
                Role role = new();
                Assert.IsFalse(role.EstDansSystemeRole(1));
                role.AjouterMembreRole(1);
                Assert.IsTrue(role.EstDansSystemeRole(1));
            }
            catch (ArgumentException) { }
        }
        #endregion

        #region Tests sur AjouterMembreRole()
        /// <summary>
        /// Teste que la méthode ajoute un membre dans le système de rôle.
        /// </summary>
        [TestMethod]
        public void TestAjouterMembreRoleSucces()
        {
            try
            {
                var membre = new Mock<IGuildUser>();
                membre.Setup(x => x.Id).Returns(10);

                Role role = new();
                Assert.IsFalse(role.EstDansSystemeRole(membre.Object.Id));
                role.AjouterMembreRole(membre.Object.Id);
                Assert.IsTrue(role.EstDansSystemeRole(membre.Object.Id));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode avec une entrée nulle.
        /// </summary>
        [TestMethod]
        public void TestAjouterMembreRoleNull()
        {
            try
            {
                Role role = new();
                int? x = null;

                Action action = () => role.AjouterMembreRole((ulong) x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }
        #endregion

        #region Tests sur RetirerMembreRole()
        /// <summary>
        /// Teste que la méthode retire un membre dans le système de rôle.
        /// </summary>
        [TestMethod]
        public void TestRetirerMembreRoleSucces()
        {
            try
            {
                var membre = new Mock<IGuildUser>();
                membre.Setup(x => x.Id).Returns(10);

                Role role = new();
                role.AjouterMembreRole(membre.Object.Id);
                Assert.IsTrue(role.EstDansSystemeRole(membre.Object.Id));
                role.RetirerMembreRole(membre.Object.Id);
                Assert.IsFalse(role.EstDansSystemeRole(membre.Object.Id));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode avec une entrée nulle.
        /// </summary>
        [TestMethod]
        public void TestRetirerMembreRoleNull()
        {
            try
            {
                Role role = new();
                int? x = null;

                Action action = () => role.RetirerMembreRole((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }
        #endregion

        #region Tests sur AjouterRole()
        /// <summary>
        /// Teste que la méthode ajoute un rôle dans la liste des rôles du membre.
        /// </summary>
        [TestMethod]
        public void TestAjouterRoleSucces()
        {
            try
            {
                var membre = new Mock<IGuildUser>();
                membre.Setup(x => x.Id).Returns(10);

                var role = new Mock<IRole>();
                role.Setup(x => x.Id).Returns(2);

                Role roles = new();
                roles.AjouterMembreRole(membre.Object.Id);
                Assert.IsFalse(roles.EstEnPossessionRole(membre.Object, role.Object.Id));
                roles.AjouterRole(membre.Object, role.Object);
                Assert.IsTrue(roles.EstEnPossessionRole(membre.Object, role.Object.Id));
            }
            catch (ArgumentException) { }
        }
        
        /// <summary>
        /// Teste le comportement de la méthode avec un rôle nul.
        /// </summary>
        [TestMethod]
        public void TestAjouterRoleNull()
        {
            try
            {
                var membre = new Mock<IGuildUser>();
                membre.Setup(x => x.Id).Returns(10);

                Role role = new();
                role.AjouterMembreRole(membre.Object.Id);
                Assert.IsTrue(role.ObtenirRoles(membre.Object.Id).Count == 0);
                role.AjouterRole(membre.Object, null);
                Assert.IsTrue(role.ObtenirRoles(membre.Object.Id).Count == 0);
            }
            catch (ArgumentException) { }
        }
        #endregion

        #region Tests sur RetirerRole()
        /// <summary>
        /// Teste que la méthode retire un rôle au membre.
        /// </summary>
        [TestMethod]
        public void TestRetirerRoleSucces()
        {
            try
            {
                var membre = new Mock<IGuildUser>();
                membre.Setup(x => x.Id).Returns(10);

                var role = new Mock<IRole>();
                role.Setup(x => x.Id).Returns(2);

                Role roles = new();
                roles.AjouterMembreRole(membre.Object.Id);
                roles.AjouterRole(membre.Object, role.Object);
                Assert.IsTrue(roles.EstEnPossessionRole(membre.Object, role.Object.Id));
                roles.RetirerRole(membre.Object, role.Object);
                Assert.IsFalse(roles.EstEnPossessionRole(membre.Object, role.Object.Id));
            }
            catch (ArgumentException) { }
        }
        
        /// <summary>
        /// Teste le comportement de la méthode avec une entrée nulle.
        /// </summary>
        [TestMethod]
        public void TestRetirerRoleNull()
        {
            try
            {
                var membre = new Mock<IGuildUser>();
                membre.Setup(x => x.Id).Returns(10);

                var role = new Mock<IRole>();
                role.Setup(x => x.Id).Returns(2);

                Role roles = new();
                roles.AjouterMembreRole(membre.Object.Id);
                roles.AjouterRole(membre.Object, role.Object);
                Assert.IsTrue(roles.ObtenirRoles(membre.Object.Id).Count == 1);
                roles.RetirerRole(membre.Object, null);
                Assert.IsTrue(roles.ObtenirRoles(membre.Object.Id).Count == 1);
            }
            catch (ArgumentException) { }
        }
        #endregion

        #region Tests sur EstEnPossessionRole()
        /// <summary>
        /// Teste que la méthode détecte bien qu'un utilisateur possède un rôle.
        /// </summary>
        [TestMethod]
        public void TestEstEnPossessionRoleSucces()
        {
            try
            {
                var membre = new Mock<IGuildUser>();
                membre.Setup(x => x.Id).Returns(10);

                var role = new Mock<IRole>();
                role.Setup(x => x.Id).Returns(1);

                var role2 = new Mock<IRole>();
                role.Setup(x => x.Id).Returns(2);

                Role roles = new();
                roles.AjouterMembreRole(membre.Object.Id);
                Assert.IsFalse(roles.EstEnPossessionRole(membre.Object, role.Object.Id));
                roles.AjouterRole(membre.Object, role.Object);
                Assert.IsTrue(roles.EstEnPossessionRole(membre.Object, role.Object.Id));
                Assert.IsFalse(roles.EstEnPossessionRole(membre.Object,
                    role2.Object.Id));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode en donnant un id de rôle nul.
        /// </summary>
        [TestMethod]
        public void TestEstEnPossessionRoleNull()
        {
            try
            {
                var membre = new Mock<IGuildUser>();
                membre.Setup(x => x.Id).Returns(10);

                int? x = null;

                Role roles = new();
                Action action = () => roles.EstEnPossessionRole(membre.Object, (ulong) x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }
        #endregion
    }
}
