﻿using Discord;
using IVY.Repertoires;
using System.Collections.Generic;
using System.Linq;

namespace IVY.Objets
{
    /// <summary>
    /// S'occupe de la gestion du système de rôle.
    /// </summary>
    public class Role: RepertoireRoles 
    {
        /// <summary>
        /// L'id du rôle négatif attribuable aux membres.
        /// </summary>
        public const ulong RoleNegatifId = 1070318619407437864;

        /// <summary>
        /// L'id du rôle positif attribuable aux membres.
        /// </summary>
        public const ulong RolePositifId = 1070318618488873052;

        /// <summary>
        /// L'id du rôle d'administrateur attribuable aux membres.
        /// </summary>
        public const ulong RoleAdminId = 1070318609454342144;

        /// <summary>
        /// Constructeur de la classe Roles.
        /// </summary>
        public Role() { }

        /// <summary>
        /// Permet de déterminer si le membre fait partie du système de rôle.
        /// </summary>
        /// <param name="membreId">L'id du membre à vérifier.</param>
        /// <returns>True si le membre est dans les rôles, false sinon.</returns>
        public bool EstDansSystemeRole(ulong membreId)
        {
            return Roles.ContainsKey(membreId);
        }

        /// <summary>
        /// Permet d'ajouter un nouveau membre au système de rôle.
        /// </summary>
        /// <param name="membreId">L'id du membre à ajouter.</param>
        public void AjouterMembreRole(ulong membreId)
        {
            Roles.Add(membreId, new List<IRole>());
        }

        /// <summary>
        /// Permet de retirer un membre du système de rôle.
        /// </summary>
        /// <param name="membreId">L'id du membre à retirer.</param>
        public void RetirerMembreRole(ulong membreId)
        {
            Roles.Remove(membreId);
        }

        /// <summary>
        /// Permet d'ajouter une rôle donné à un membre donné.
        /// </summary>
        /// <param name="membre">Le membre qui reçoit un rôle.</param>
        /// <param name="role">Le rôle à ajouter.</param>
        public async void AjouterRole(IGuildUser membre, IRole role)
        {
            if (!EstEnPossessionRole(membre, role.Id))
            {
                await membre.AddRoleAsync(role);
                Roles[membre.Id].Add(role);
            }
        }

        /// <summary>
        /// Permet de retirer un rôle donné à un membre donné.
        /// </summary>
        /// <param name="membre">Le membre qui perd un rôle.</param>
        /// <param name="role">Le rôle à retirer.</param>
        public async void RetirerRole(IGuildUser membre, IRole role)
        {
            if (EstEnPossessionRole(membre, role.Id))
            {
                await membre.RemoveRoleAsync(role);
                Roles[membre.Id].Remove(role);
            }
        }

        /// <summary>
        /// Permet de déterminer si le rôle donné est attribué au membre donné.
        /// </summary>
        /// <param name="membreId">Le membre à vérifier.</param>
        /// <param name="roleId">L'id du rôle à vérifier.</param>
        /// <returns>True si le rôle est attribué au membre, false sinon.</returns>
        public bool EstEnPossessionRole(IGuildUser membre, ulong roleId)
        {
            return Roles[membre.Id].Any(x => x.Id == roleId);
        }
    }
}
