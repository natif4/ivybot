﻿using Discord;

namespace IVY.Objets
{
    /// <summary>
    /// La classe Reaction s'occupe de la gestion des réactions aux messages 
    /// dans le système de pointage.
    /// </summary>
    public class Reaction
    {
        /// <summary>
        /// Représente le coeur rouge, vu ici comme une appréciation.
        /// </summary>
        protected readonly Emoji Appreciation = new("❤️");

        /// <summary>
        /// Représente le drapeau rouge triangulaire, vu ici comme une plainte.
        /// </summary>
        protected readonly Emoji Plainte = new("🚩");

        /// <summary>
        /// Constructeur de la classe Reaction.
        /// </summary>
        public Reaction() { }

        /// <summary>
        /// Permet de déterminer si la réaction donnée est une appréciation.
        /// </summary>
        /// <param name="reaction">Le nom de la réaction en string.</param>
        /// <returns>True s'il s'agit d'une appréciation, flase sinon.</returns>
        public bool EstAppreciation(string reaction)
        {
            return (reaction == Appreciation.Name);
        }

        /// <summary>
        /// Permet de déterminer si la réaction donnée est une plainte.
        /// </summary>
        /// <param name="reaction">Le nom de la réaction en string.</param>
        /// <returns>True s'il s'agit d'une plainte, flase sinon.</returns>
        public bool EstPlainte(string reaction)
        {
            return (reaction == Plainte.Name);
        }

        /// <summary>
        /// Permet d'obtenir l'emoji d'appréciation.
        /// </summary>
        /// <returns>L'emoji d'appréciation.</returns>
        public Emoji ObtenirAppreciation()
        {
            return Appreciation;
        }

        /// <summary>
        /// Permet d'obtenir l'emoji de plainte.
        /// </summary>
        /// <returns>L'emoji de plainte.</returns>
        public Emoji ObtenirPlainte()
        {
            return Plainte;
        }
    }
}
