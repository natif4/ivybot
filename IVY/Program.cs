﻿using System.IO;
using System.Threading.Tasks;

namespace IVY
{
    class Program
    {
        /// <summary>
        /// Permet le démarrage du programme.
        /// </summary>
        public static void Main()
        {
            // On tente ici d'aller obtenir le token dans le fichier donné ou
            // on envoie en affichage console l'exception si levée: le token
            // est stocké dans un fichier .txt contenant uniquement la chaîne
            // de caractères du token. 
            StreamReader sr = new("C:\\TokenBot.txt");
            var token = sr.ReadLine();
            Ivy ivy = new(token);
            ivy.LancementEnAttente();
            Task.Delay(-1).Wait();
        }
    }
}
